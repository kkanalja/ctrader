<?php
define("DATA_DIR",'/home/examples/websocket/spot/_klines/binance/spot/'); // папка с файлами для анализа
define("SIGNAL_FILE",__DIR__.'/signal.txt');
define("RESULT_FILE",__DIR__.'/result.txt');
define("LOG_FILE",__DIR__.'/output.log');
define("LOG_SCREEN",true);
define("LOG_DEPTH",1); //VERBOSE 1 or 2
define("TIME_PERIODS",['1s','1h','4h','15m','5m','1m']); //Виды временных интервалов для учета вариантов работы парсера
define("CACHE_FILE",__DIR__.'/cache.json');