<?php


class CTrader
{
    protected $signal_file;
    protected $signal;
    protected $data;
    protected $last_result;
    protected $current_function;
    protected $file_cache=[];

    public function __construct(string $signal_file)
    {
        $this->signal_file = $signal_file;
        if(!file_exists(CACHE_FILE))file_put_contents(CACHE_FILE,'{}');
        $this->file_cache = json_decode(file_get_contents(CACHE_FILE),1);
    }

    protected function log($text)
    {
        if(LOG_DEPTH==0)return;
        $string=$text."\n";
        file_put_contents(LOG_FILE,$string,FILE_APPEND);
        if(LOG_SCREEN)echo $string;
    }

    protected function sendPOST(string $function,array $args)
	{
		$link='http://149.154.66.185/api/php_function/index.php';
        $var=array_merge([$function],$args);
	    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
	    #Устанавливаем необходимые опции для сеанса cURL
	    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($curl,CURLOPT_URL,$link);
	    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
	    $postfields= http_build_query($var);
	    curl_setopt($curl,CURLOPT_POSTFIELDS,$postfields);
	    curl_setopt($curl,CURLOPT_HEADER,false);
	    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
	    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
	    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
	    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
	    curl_close($curl); #Завершаем сеанс cURL
	    $Response=json_decode($out,true);
	    return $Response;
	}

    protected function validate_data(array $data)
    {
        $tmp=json_decode($data[0],1);
        $first=$tmp['k']['T'];
        foreach($data as $index=>$d)
        {
        //var_dump($d);
            $tmp=json_decode($d,1);
            if(!$tmp)continue;
            if($index>=1)$second=$tmp['k']['t'];
            if(($index>=1) && ($second-$first)!=1)
            {
                var_dump($tmp);
                return false;
            }
            if($index>=1)$first=$tmp['k']['T'];
        }
        return true;
    }

    protected function callTrade(string $function,string $args,array $data)
    {
        preg_match("/(.+)([1-3])$/",$function,$matches);
        if(isset($matches[0]))
        {
            $index=$matches[2];
            $function=$matches[1];
        }
        $function="trader_".$function;
        $args=explode(' ',trim($args));
        $args=$args[0];
        $args=explode('/',$args);
        $arguments=[$data];
        $arguments=array_merge($arguments,$args);
        if(function_exists($function))
        {
            $this->current_function="local";
            $res=call_user_func_array($function,$arguments);
        }
        else{
            $this->current_function="remote";
            $res=$this->sendPOST($function,$arguments);
        }
        //Вывод лога по функции
        $args=implode(',',$args);
        if(LOG_DEPTH==1)$this->log("$function(data, $args) ($this->current_function)");
        elseif(LOG_DEPTH==2)
        {
            $data_text=print_r($data,1);
            $result=json_encode($res,1);
            $this->log("$function($data_text, $args) ($this->current_function)\n response=$result");
        }
        if(isset($index))
        {
            $this->last_result=$res;
            return $res[$index-1];
        }
        return $res;   
    }

    protected function parse_signal()
    {
        $data=[];
        $text=file_get_contents($this->signal_file);
        $text=preg_split("#\n\s*\n#Uis" , $text);;
        foreach($text as $val)
        {
            if(!trim($val))continue;
            $block=explode("\n",$val);
            $block_text=trim($val);
            $block_info=explode(' ',trim($block[0]));
            $pair_mask=trim($block[1]);
            $tmp=['id'=>$block_info[0] ?? 0,'pos'=>$block_info[1] ?? '','pairs'=>[],'block_text'=>$block_text];
            foreach($block as $key=>$b)
            {
                $b=trim($b);
                if($key<=1)continue;
                $ss=explode('/',$b);

                if(1)
                {
                    $str=$b;
                    $type=$ss[1];
                    $function=$ss[2];
                    $args=explode('/'.$function.'/',$b);
                    $args=$args[1];
                    $search_mask=str_replace('_','',$pair_mask);
                    $search_dir=DATA_DIR.$ss[0].'/'.$search_mask;
                    $files=glob($search_dir);
                    $index=$key-1;
                    foreach($files as $data_file)
                    {
                        $pair=basename($data_file);
                        $tmp['pairs'][]=compact('data_file','type','function','args','pair','index','str');
                    }
                }else{
                    die ("Ошибки при парсинга файла с сигналами\n");
                }

            }
            $data[]=$tmp;
        }
        return $data;
    }


    protected function getData(string $filename = __DIR__ . '/_klines/binance/spot/1m/btcusdt', $field = 'k/c')
    {
        if(!file_exists($filename))return false;
        $r = trim(file_get_contents($filename));

        $e = explode("\n", $r);
        $f = explode('/', $field);
        $data = [];
        if(!$this->validate_data($e))return false;
        foreach ($e as $v) {
            $a = json_decode($v, 1);
            $tmp = $a;
            foreach ($f as $vv) {
                $tmp = $tmp[$vv];
            }
            $data[] = $tmp;
        }
        return $data;
    }

    protected function check_ftime($data)
    {
        $files=array_column($data,'data_file');
        $changed=false;
        foreach($files as $file)
        {
            $time=filemtime($file);
            $date=date('Y-m-d H:i:s');
            if($time!=($this->file_cache[$file] ?? null))
            {
                $this->file_cache[$file]=$time;
                $this->save_cache_data();
                $changed=true;
                $this->log("filemtime $date, $file updated ok");
            }
            else $this->log("filemtime $date, $file not updated");
        }
        return $changed;
    }

    protected function check(string $arg,array $result,array $data)
    {
        $a=explode(' ',trim($arg));
        $ex=$a[2];
        if(in_array($ex,['open','close','low','high']))
        {
            $last=array_column($data,$ex[0]);
            $last=array_pop($last);
            $ex=$last;
        }elseif(!is_numeric($ex))   //Если последнее слово не число, то подразумеваем что это типа bband1
        {
            preg_match("/(.+)([1-3])$/",$ex,$matches);
            $index=$matches[2];
            $result2=$this->last_result[$index];
            $count1=count($result);
            $count2=count($result2);
            echo $this->log($result[$count1-1].' '. $result2[$count2-1].' '.$result[$count1-2].' '.$result2[$count2-2]);
            if($result[$count1-1] < $result2[$count2-1] && $result[$count1-2] > $result2[$count2-2])return true;
            else return false;
        }
        $c = count($result);
        //print_r($result);
       // echo "ex=$ex";
        //echo " Last = ".$result[$c-1];
        //echo " Prev Last = ".$result[$c-2]."\n";
        if($a[1]=='>')
        {
            $flag=($result[$c-1] > $ex) && ($result[$c-2] < $ex);
            if(!$flag)$res='false';else $res='true';
            $this->log('condition: '.$result[$c-1].' > '.$ex.' && '.$result[$c-2].' < '.$ex.' = '.$res);
        }
        elseif($a[1]=='<')
        {
            $flag=($result[$c-1] < $ex) && ($result[$c-2] > $ex);
            if(!$flag)$res='false';else $res='true';
            $this->log('condition: '.$result[$c-1].' < '.$ex.' && '.$result[$c-2].' > '.$ex.' = '.$res);
        }
        return $flag;
    }

    protected function write_file(array $array)
    {
        $json=json_encode($array);
        file_put_contents(RESULT_FILE,"$json\n", FILE_APPEND);
    }

    protected function save_cache_data()
    {
        file_put_contents(CACHE_FILE,json_encode($this->file_cache));
    }

    protected function print_block($block)
    {
        $this->log("$block[block_text]\n");
    }
    
    protected function print_pair($pair, $header)
    {
        $this->log(date('Y-m-d H:i:s')."\n$header");
        $res=array_map(function($a){
               return "$a[pair]/$a[str]";
        },
               $pair);
        $res=implode("\n",$res);
        $this->log("$res");
    }

    protected function sort_pairs($pairs)
    {
        $result=[];
        foreach($pairs as $pair)
        {
            $result[$pair['pair']][]=$pair;
        }
        return $result;
    }

    /**
     * Проверяет на изменение всех файлов в сигналах
     *
     * @param array $signals_data
     * @return array
     */
    protected function check_files($signals_data)
    {
        $result=[];
        foreach($signals_data as $data)
        {
                $files=array_column($data['pairs'],'data_file');
                $files=array_unique($files);
                $result=array_merge($files,$result);
        }
        $result=array_unique($result);
        foreach($result as $file)
        {
            $time=filemtime($file);
            if($time!=($this->file_cache[$file]['change_time'] ?? null))
            {
                //echo "$file changed\n";
                $this->file_cache[$file]['change_time']=$time;
                $this->file_cache[$file]['changed']=true;
            }
            else $this->file_cache[$file]['changed']=false;
        }
        $this->save_cache_data();
    }

    protected function check_files_block($block)
    {
        $files=array_column($block,'data_file');
        $flag=false;
        foreach($files as $file)
        {
            $date=date('Y-m-d H:i:s',$this->file_cache[$file]['change_time']);
            if($this->file_cache[$file]['changed'])return "$file filemtime updated $date";
        }
        return $flag;
    }

    public function procces()
    {
        $this->data=$this->parse_signal();
        $data=$this->data;
        $this->check_files($data);
        foreach($data as $d)
        {
            $id=$d['id'];
            $pos=$d['pos'];
            $flag=false;
            $this->log(date('Y-m-d H:i:s')." begin signal_id=$id");
            $pairs=$this->sort_pairs($d['pairs']);
            $this->print_block($d);
            /*
            $changed_file=$this->check_files_block($d);
            if(!$changed_file)
            {
                $this->log("stop\n");
                continue;
            }
            */
            $current=[];
            foreach($pairs as $i=>$pair_block)
            {
                $this->print_pair($pair_block,"$id $pos");
                $current_res=false;
                $changed_file=$this->check_files_block($pair_block);
                if($changed_file)$this->log("$changed_file\nПеречитываем файлы");
                else 
                {
                    $this->log("stop Ни один файл не обновился\n");
                    continue;
                }
                foreach($pair_block as $pair)
                {
                    $file=$pair['data_file'];
                    $type=$pair['type'];
                    $pair_name=$pair['pair'];
                    $index=$pair['index'];
                    $function=$pair['function'];
                    $args=$pair['args'];
                    $this->log("Считывается файл $file");
                    $data_column=$this->getData($file,'k');
                    if(!$data_column)
                    {
                        $this->log("Ошибка получения данных из файла $file\n");
                        die();
                    }
                    $close_price=array_column($data_column,'c');
                    $close_price=array_pop($close_price);
                    $result=$this->callTrade($function,$args,array_column($data_column,$type[0]));
                    if(!is_array($result))
                    {
                        $this->log("Ошибка получения данных из функции $function\n");
                        die();
                    }
                    if(count($result)==0)continue;
                    $result=array_values($result);
                    //Проверка  пробития уровня индикатора, заданного в сигнале.
                    $current_res=$this->check($args,$result,$data_column);
                    if(!$current_res)break;
                }
                if($current_res)
                {
                    echo "Сработка\n";
                    $a = ['ts' => time(), 'dt' => date('Y-m-d H:i:s'), 'signal_id' => $d['id'], 'pair' => $pair_name, 'price' => $close_price, 'positionSide' => $pos];
                    $this->write_file($a);
                }else $a=[];
                $this->log("result: ".json_encode($a)."\n");
               
            }


        }
     
    }

}

function errRaise($errNo, $errStr, $errFile, $errLine) {
    $msg = "$errStr in $errFile on line $errLine";
        throw new ErrorException($msg, $errNo);
}

set_error_handler('errRaise');
